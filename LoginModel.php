<?php
class LoginModel extends CI_Model
{
	function __construct() {
		parent::__construct();
	}
        
	function login($email,$password)
	{
		$this -> db->select(' * ');
		$this -> db->from('salespersons');
		$this -> db->where('email', $email);
		$this -> db->where('password', $password);
		$this -> db->limit(1);
		$query = $this->db-> get();
		return $query;
	}
	
	function getSalesPersons($email){
		$this -> db->select(' * ');
		$this -> db->from('salespersons');
		$this -> db->where('email', $email);
		$this -> db->limit(1);
		$query = $this->db-> get()->row_array();
		return $query;
		
	}
}